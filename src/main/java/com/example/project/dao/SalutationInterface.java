package com.example.project.dao;

import org.springframework.data.repository.CrudRepository;

import com.example.project.model.SalutationTableDemo;

public interface SalutationInterface extends CrudRepository<SalutationTableDemo,Integer> {

}

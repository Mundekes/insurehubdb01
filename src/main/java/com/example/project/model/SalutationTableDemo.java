package com.example.project.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class SalutationTableDemo {
	@Id
	private int id;
	private String SalutationTitle;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSalutationTitle() {
		return SalutationTitle;
	}
	public void setSalutationTitle(String salutationTitle) {
		SalutationTitle = salutationTitle;
	}
	@Override
	public String toString() {
		return "SalutationTableDemo [id=" + id + ", SalutationTitle=" + SalutationTitle + "]";
	}

}

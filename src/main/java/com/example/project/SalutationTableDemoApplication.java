package com.example.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SalutationTableDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SalutationTableDemoApplication.class, args);
	}

}

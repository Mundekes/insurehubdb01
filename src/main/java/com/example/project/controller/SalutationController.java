package com.example.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.project.dao.SalutationInterface;
import com.example.project.model.SalutationTableDemo;
@Controller
public class SalutationController {
	@Autowired
	SalutationInterface repo;
	
	@RequestMapping("/")
	public String home()
	{
		return "home.jsp";
	}
	
	@RequestMapping("/Add")
	public String Add(SalutationTableDemo salutation) {
		
		repo.save(salutation);
		System.out.println("hi");
		return "home.jsp";
	}
	@RequestMapping("/getSalutationType")
	public ModelAndView getSalutationType(@RequestParam int id) {
		
		ModelAndView mv = new ModelAndView("show.jsp");
		SalutationTableDemo salutation =repo.findById(id).orElse(new SalutationTableDemo());
		mv.addObject("salutation",salutation);
	
		return mv;
	}
	
	@RequestMapping("/viewSalution")
	public ModelAndView viewSalution(SalutationTableDemo salutation) {
		
		ModelAndView mv = new ModelAndView("view.jsp");
		Iterable<SalutationTableDemo> salutation1  =  repo.findAll();
		mv.addObject("salutation",salutation1);
		return mv;
	}
	
	
	
	}


